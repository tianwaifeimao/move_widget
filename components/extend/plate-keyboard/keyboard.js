// components/extend/plate-keyboard/keyboard.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    type: {
      type: Number,
      value: 1
    },
    show:{
      type:Boolean,
      value:true
    },
    vibrate:{
      type:Boolean,
      value:true
    },
    themeColor:{
      type: String,
      value: '#09f'
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    keyboardProvince: [
      ['京', '津', '沪', '渝', '冀', '豫', '云', '辽', '黑', '湘'],
      ['皖', '鲁', '新', '苏', '浙', '赣', '鄂', '桂', '甘', '晋'],
      ['蒙', '陕', '吉', '闽', '贵', '粤', '青', '藏'],
      ['川', '宁', '琼', '港', '澳', '台']
    ],
    keyboardValue: [
      [1, 2, 3, 4, 5, 6, 7, 8, 9, 0],
      ['Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P'],
      ['A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L'],
      ['Z', 'X', 'C', 'V', 'B', 'N', 'M']
    ]
  },

  /**
   * 组件的方法列表
   */
  methods: {
    hitKeycap: function (e) {
      const i = e.currentTarget.dataset.i;
      const j = e.currentTarget.dataset.j;
      let value = ''
      if (this.data.type == 1) {
        value = this.data.keyboardProvince[i][j]

      } else {
        value = this.data.keyboardValue[i][j]
      }
      if (this.data.vibrate) {
        wx.vibrateShort();
      };
      this.emit(value);
      // that.$emit('hit', value)
    },
    hitDelete: function () {
      if (this.data.vibrate) {
        wx.vibrateShort();
      };
      this.emit('delete');
    },
    complete: function () {
      this.emit('complete');
    },
    emit: function (value) {
      this.triggerEvent('hit', { value }, {})
    }
  }
})