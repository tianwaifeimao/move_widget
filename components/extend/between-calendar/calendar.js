// components/extend/calendar/calendar.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    themeColor: {
      type: String,
      value: '#ff4400'
    },
    paddindTop: {
      type: String,
      value: '80rpx'
    },
    height: {
      type: String,
      value: '100vh'
    },
    initMonthCount: {
      type: Number,
      value: 6
    },
    date: {
      type: String,
      value: ''
    },
    startDate: {
      type: String,
      value: ''
    },
    endDate: {
      type: String,
      value: ''
    },
    disabledList: {
      type: Array,
      value: []
    },
    mode: {
      type: Number,
      value: 1
    },
    allAbled: {
      type: Boolean,
      value: false
    },
  },

  /**
   * 组件的初始数据
   */
  data: {
    //
    endDates: '',
    startDates: '',
    monthCount: '',
    dates: '',
    currentMonthNum: 0, //当前月的索引，switchMonth=true时使用
    isDate: false,
    selectPrice: [], //保存选择的日期所在的价格
    weekList: ['日', '一', '二', '三', '四', '五', '六'],
    calendar: [],
    today: 0,
    lastDate: 0,
    year: 0,
    month: 0,
    betweenColor: '',
    festival: {
      "1/1": "元旦",
      "2/14": "情人节",
      "3/8": "妇女节",
      "4/1": "愚人节",
      "5/1": "劳动节",
      "6/1": "儿童节",
      "7/1": "建党节",
      "8/1": "建军节",
      "9/10": "教师节",
      "10/1": "国庆节",
      "11/26": "感恩节",
      "12/24": "平安夜",
      "12/25": "圣诞节",
    },
    lunarFestival: {
      "1/1": '春节',
      "1/15": "元宵节",
      "5/5": "端午节",
      "7/7": "七夕",
      "7/15": "中元节",
      "8/15": "中秋节",
      "9/9": "重阳节",
      "12/8": "腊八节",
    },
    lunarYearArr: [
      0x0b557, //1949
      0x06ca0, 0x0b550, 0x15355, 0x04da0, 0x0a5b0, 0x14573, 0x052b0, 0x0a9a8, 0x0e950, 0x06aa0, //1950-1959
      0x0aea6, 0x0ab50, 0x04b60, 0x0aae4, 0x0a570, 0x05260, 0x0f263, 0x0d950, 0x05b57, 0x056a0, //1960-1969
      0x096d0, 0x04dd5, 0x04ad0, 0x0a4d0, 0x0d4d4, 0x0d250, 0x0d558, 0x0b540, 0x0b6a0, 0x195a6, //1970-1979
      0x095b0, 0x049b0, 0x0a974, 0x0a4b0, 0x0b27a, 0x06a50, 0x06d40, 0x0af46, 0x0ab60, 0x09570, //1980-1989
      0x04af5, 0x04970, 0x064b0, 0x074a3, 0x0ea50, 0x06b58, 0x055c0, 0x0ab60, 0x096d5, 0x092e0, //1990-1999
      0x0c960, 0x0d954, 0x0d4a0, 0x0da50, 0x07552, 0x056a0, 0x0abb7, 0x025d0, 0x092d0, 0x0cab5, //2000-2009
      0x0a950, 0x0b4a0, 0x0baa4, 0x0ad50, 0x055d9, 0x04ba0, 0x0a5b0, 0x15176, 0x052b0, 0x0a930, //2010-2019
      0x07954, 0x06aa0, 0x0ad50, 0x05b52, 0x04b60, 0x0a6e6, 0x0a4e0, 0x0d260, 0x0ea65, 0x0d530, //2020-2029
      0x05aa0, 0x076a3, 0x096d0, 0x04afb, 0x04ad0, 0x0a4d0, 0x1d0b6, 0x0d250, 0x0d520, 0x0dd45, //2030-2039
      0x0b5a0, 0x056d0, 0x055b2, 0x049b0, 0x0a577, 0x0a4b0, 0x0aa50, 0x1b255, 0x06d20, 0x0ada0, //2040-2049
      0x14b63, 0x09370, 0x049f8, 0x04970, 0x064b0, 0x168a6, 0x0ea50, 0x06b20, 0x1a6c4, 0x0aae0, //2050-2059
      0x0a2e0, 0x0d2e3, 0x0c960, 0x0d557, 0x0d4a0, 0x0da50, 0x05d55, 0x056a0, 0x0a6d0, 0x055d4, //2060-2069
      0x052d0, 0x0a9b8, 0x0a950, 0x0b4a0, 0x0b6a6, 0x0ad50, 0x055a0, 0x0aba4, 0x0a5b0, 0x052b0, //2070-2079
      0x0b273, 0x06930, 0x07337, 0x06aa0, 0x0ad50, 0x14b55, 0x04b60, 0x0a570, 0x054e4, 0x0d160, //2080-2089
      0x0e968, 0x0d520, 0x0daa0, 0x16aa6, 0x056d0, 0x04ae0, 0x0a9d4, 0x0a2d0, 0x0d150, 0x0f252, //2090-2099
      0x0d520 //2100
    ],
    lunarMonth: ['正', '二', '三', '四', '五', '六', '七', '八', '九', '十', '冬', '腊'],
    lunarDay: ['一', '二', '三', '四', '五', '六', '七', '八', '九', '十', '初', '廿'],
    tianGan: ['甲', '乙', '丙', '丁', '戊', '己', '庚', '辛', '壬', '癸'],
    diZhi: ['子', '丑', '寅', '卯', '辰', '巳', '午', '未', '申', '酉', '戌', '亥']
  },

  /**
   * 组件的方法列表
   */
  attached: function() {
    this.init();

  },
  methods: {
    init: function() {
      this.sloarToLunar(23, 8, 2019);
      if (this.data.initMonthCount < 1) {
        this.setData({
          monthCount: 1
        })
        console.warn("initMonthCount属性设置不能小于1")
      } else {
        this.setData({
          monthCount: this.data.initMonthCount
        })
      }
      if (this.data.date) {
        //disableDate用于addClassName方法preDisabled==true的时候使用
        const dates = new Date(this.date.replace(/-/g, '/'));
        this.setData({
          dates: dates,
          disableDate: dates,
          isDate: true,
        })
      }
      if (this.data.startDate) {
        const startDate = new Date(this.data.startDate.replace(/-/g, '/'));
        this.setData({
          startDates: startDate,
          disableStartDate: startDate
        })
      }
      if (this.data.endDate) {
        const endDate = new Date(this.endDate.replace(/-/g, '/'));
        this.setData({
          endDates: endDate
        })
      }
      this.setData({
        today: new Date(new Date().getFullYear() + '/' + (new Date().getMonth() + 1) + '/' + new Date().getDate()) * 1
      })

      if (this.data.date && (this.data.startDate || this.data.endDate)) {
        console.warn(':date属性和 (:startDate,:endDate) 不能同时设置')
        this.setData({
          isDate: true
        })
      }
      if (!this.data.date && !this.data.startDate && this.data.endDate) {
        //disableStartDate用于addClassName方法preDisabled==true的时候使用
        this.setData({
          startDates: new Date(this.data.today * 1),
          disableStartDate: new Date(this.data.today * 1)
        })
      }
      if (!this.data.date && !this.data.startDate && !this.data.endDate) {
        this.setData({
          dates: new Date(this.today * 1),
          isDate: true
        })
      }
      this.setData({
        lastDate: this.data.today + this.data.monthCount * 30 * 24 * 3600 * 1000
      });
      if (this.data.date || this.data.startDate) {
        this.setData({
          year: new Date(this.data.dates * 1 || this.data.startDates * 1).getFullYear(),
          month: new Date(this.data.dates * 1 || this.data.startDates * 1).getMonth() + 1
        })

      } else if (this.endDate) {
        console.warn("请设置先startDate")
        const today = new Date();
        this.setData({
          endDates: this.data.today * 1,
          year: today.getFullYear(),
          month: new Date().getMonth() + 1
        })
      } else {
        const today = new Date();
        this.setData({
          year: today.getFullYear(),
          month: new Date().getMonth() + 1
        })
      }

      //如果初始化date或者startDate之前月份数据
      if (parseInt(this.data.initPreMonthCount) > 0) {
        this.initPreMonth()
      }
      this.createClendar(); //创建日历数据
      this.getBetweenColor();
    },
    getBetweenColor: function() {
      let hex = this.data.themeColor
      if (hex.length == 4) {
        hex = `#${hex[1]}${hex[1]}${hex[2]}${hex[2]}${hex[3]}${hex[3]}`
      }
      let str = "rgba(" + parseInt("0x" + hex.slice(1, 3)) + "," + parseInt("0x" + hex.slice(3, 5)) + "," + parseInt("0x" + hex.slice(5, 7)) + ",0.1)";
      this.setData({
        betweenColor: str
      })
    },
    //初始化date或者startDate之前几个月的日历数据
    initPreMonth: function() {
      let year = this.data.year;
      let month = this.data.month - this.data.initPreMonthCount
      var m = Math.ceil(month / 12)
      this.monthCount = parseInt(this.data.monthCount) + parseInt(this.data.initPreMonthCount)

      if (m > 0) {
        year += m - 1
      } else {
        year += m - 1
      }
      if (month > 12) {
        month = month % 12 == 0 ? 12 : month % 12;
      }

      if (month <= 0) {
        month = 12 + month % 12
      }
      this.setData({
        year: year,
        month: month
      })
    },
    //根据当天和结束日期创建日历数据
    createClendar(flag = null) {
      for (let i = 0; i < this.data.monthCount; i++) {
        let month = this.data.month + i + this.data.currentMonthNum,
          year = this.data.year,
          _monthData = {
            dayList: [],
            month: '',
            year: ''
          };

        var m = Math.ceil(month / 12)
        if (m > 0) {
          year += m - 1
        } else {
          year += m - 1
        }
        if (month > 12) {
          month = month % 12 == 0 ? 12 : month % 12;
        }

        if (month <= 0) {
          month = 12 + month % 12
        }

        _monthData.year = year;
        _monthData.month = month;
        _monthData.dayList = this.createDayList(month, year);

        for (let i in _monthData.dayList) {
          const param = [_monthData.dayList[i].day, _monthData.month, _monthData.year];
          const calendarItemClassName = this.addCalendarItemClassName(...param);
          const tip = this.setTip(...param);
          _monthData.dayList[i] = Object.assign(_monthData.dayList[i], {
            className: calendarItemClassName,
            tip: tip,
            isWeekend: this.isWeekend(...param)
          })
        }

        let calendar = this.data.calendar;
        calendar.push(_monthData);

        this.setData({
          calendar: calendar
        })
      }
    },
    //创建每个月日历数据，传入月份1号前面用null填充
    createDayList: function(month, year) {
      const count = this.getDayNum(month, year),
        _week = new Date(year + '/' + month + '/1').getDay();
      let dayList = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28];
      let list = [];
      for (let i in dayList) {
        list.push({
          day: dayList[i]
        })
      }

      for (let i = 29; i <= count; i++) {
        list.push({
          day: i
        })
      }
      for (let i = 0; i < _week; i++) {
        list.unshift({
          day: null
        })
      }
      return list;
    },
    isWeekend: function(day, month, year) {
      const weekend = new Date(year + '/' + month + '/' + day).getDay();
      if (weekend == 0 || weekend == 6) {
        return true;
      } else {
        return false;
      }

    },
    //计算传入月份有多少天
    getDayNum: function(month, year) {
      let dayNum = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

      if (this.isLeapYear(year)) {
        dayNum[1] = 29;
      }
      return dayNum[month - 1]
    },
    //判断是否是新历闰年
    isLeapYear: function(year) {
      if ((year % 4 === 0) && (year % 100 !== 0) || (year % 400 === 0)) {
        return true;
      }
      return false;
    },
    //判断是否是清明节
    isQingmingFestival: function(day, month, year) {
      if ((day == 4 || day == 5) && month == 4) {
        if (this.isLeapYear(year) || this.isLeapYear(year - 1)) {
          if (day == 5) {
            return true
          }
        } else {
          if (day == 4) {
            return true
          }
        }
      }
      return false;
    },
    //获取普通日期选中样式背景
    getBackground: function(day, month, year) {
      const _date = new Date(year + '/' + month + '/' + day)

      if (_date * 1 == this.data.dates * 1) {
        console.log(_date * 1)
        return this.data.themeColor
      }
    },
    addCalendarItemClassName(day, month, year) {
      if (!day) {
        return '';
      }
      const _date = new Date(year + '/' + month + '/' + day)
      let className = []
      if (_date * 1 === this.data.today) {
        className.push('today');
      }
      if (!this.data.allAbled || this.data.allAbled == 'false') {
        if (this.data.initPreMonthCount > 0) {
          var tempDate = new Date(this.data.year + '/' + this.data.month + '/01') * 1
          if (_date * 1 < tempDate || _date * 1 > this.data.lastDate) { //当天之前和180天之后不可选
            className.push('disabled')
          }
        } else {
          if (_date * 1 < this.data.today || _date * 1 > this.data.lastDate) { //当天之前和180天之后不可选
            className.push('disabled')
          }
        }
      }
      //设置不允许操作的日期
      if (this.data.disabledList.length > 0) {
        var notTemp = this.data.disabledList.map(item => {
          return new Date(item) * 1
        })
        if ((notTemp).includes(new Date(_date) * 1)) {
          className.push('disabled')
        }
      }

      if (_date * 1 === this.data.dates * 1) {
        className.push('clicktime');
      }
      //preDisabled==true时设置小于disableDate的都disable
      if ((this.data.preDisabled || this.data.preDisabled == 'true') && this.data.isDate && _date * 1 < this.data.disableDate * 1) {
        className.push('disabled')
      }
      if ((this.data.preDisabled || this.data.preDisabled == 'true') && !this.data.isDate && _date * 1 < this.data.disableStartDate * 1) {
        className.push('disabled')
      }
      return className.join(' ');
    },
    //设置今天，明天，后天
    setTip: function(day, month, year) {
      if (!day) {
        return;
      }
      const td = year + '/' + month + '/' + day
      const _date = new Date(td) * 1

      let tip;
      tip = this.data.festival[month + "/" + day];
      const lunarResult = this.sloarToLunar(day, month, year);
      tip = lunarResult.lunarDay;
      tip = this.data.lunarFestival[lunarResult.monthSpan + "/" + lunarResult.daySpan] || lunarResult.lunarDay;
      if (this.isQingmingFestival(day, month, year)) {
        tip = '清明节';
      }
      if (lunarResult.daySpan == 1) {
        tip = lunarResult.lunarMonth + '月';
      }
      if (_date == this.data.today) {
        tip = '今天';
      } else if (_date - this.data.today === 24 * 3600 * 1000) {
        tip = '明天';
      } else if (_date - this.data.today === 2 * 24 * 3600 * 1000) {
        tip = '后天'
      }

      if (!this.data.date && (this.data.startDates || this.data.endDates)) {
        if (_date === this.data.startDates * 1) {
          if (this.data.mode == 2) {
            if (this.data.endDates * 1 == 0) {
              tip = '去/返'
            } else {
              tip = '去程'
            }
          } else {
            tip = '入住'
          }

        } else if (_date === this.data.endDates * 1) {
          if (this.data.mode == 2) {
            tip = '返程'
          } else {
            tip = '离开'
          }
        }
      }

      return tip;
    },
    dateFormat: function(times) {
      let date = new Date(times);
      return {
        year: date.getFullYear(),
        month: parseInt(date.getMonth() + 1) > 9 ? parseInt(date.getMonth() + 1) : '0' + parseInt(date.getMonth() + 1),
        day: date.getDate() > 9 ? date.getDate() : '0' + date.getDate(),
        week: this.data.weekList[date.getDay()]
      }
    },
    chooseDate: function(e) {
      const day = e.currentTarget.dataset.day;
      const month = e.currentTarget.dataset.month;
      const year = e.currentTarget.dataset.year;
      if (!day) {
        return;
      }
      const _date = new Date(year + '/' + month + '/' + day) * 1
      if (!this.data.allAbled || this.data.allAbled == 'false') {
        if (this.data.initPreMonthCount > 0) {
          var tempDate = new Date(this.data.year + '/' + this.data.month + '/01') * 1
          if (_date * 1 < tempDate || _date * 1 > this.data.lastDate) { //当天之前和180天之后不可选
            return
          }
        } else {
          //超出180天范围之前和之后disable灰色的区域不可点击
          if (_date < this.data.today || _date > this.data.lastDate) {
            return;
          }
        }
      }
      //如果设置preDisabled==true，小于disableDate的日期都不能点击
      if ((this.data.preDisabled || this.data.preDisabled == 'true') && this.data.isDate && _date * 1 < this.data.disableDate * 1) {
        return;
      }
      if ((this.data.preDisabled || this.data.preDisabled == 'true') && !this.data.isDate && _date * 1 < this.data.disableStartDate * 1) {
        return;
      }

      //设置不允许操作的日期
      if (this.data.disabledList.length > 0) {
        var notTemp = this.data.disabledList.map(item => {
          return new Date(item) * 1
        })
        if (notTemp.includes(_date)) {
          return;
        }
      }

      if (_date == this.data.today || this.data.dates * 1) {
        this.setData({
          dates: _date
        })
      }
      console.log(this.data.startDates * 1);
      console.log(this.data.endDates * 1);
      if (this.data.startDates * 1 == 0 && this.data.endDates * 1 == 0) {
        this.setData({
          startDates: _date,
          endDates: ''
        })
      } else if (this.data.startDates * 1 && this.data.endDates * 1 && _date > this.data.endDates * 1) {
        console.log(1);
        this.setData({
          startDates: _date,
          endDates: ''
        })
      } else if (this.data.endDates * 1 && _date > this.data.endDates) {
        console.log(2);
        this.setData({
          endDates: _date
        })
      } else if (_date >= this.data.startDates * 1 && _date <= this.data.endDates * 1) {
        console.log(3);
        this.setData({
          startDates: _date,
          endDates: ''
        })
      } else if (_date < this.data.startDates * 1) {
        console.log(4);
        this.setData({
          startDates: _date,
          endDates: ''
        })
      } else if (_date > this.data.startDates * 1) {
        console.log(5);
        this.setData({
          endDates: _date
        })
      }

      const dateChoose = this.dateFormat(this.dates)
      const choose = {
        dateTime: this.data.dates * 1,
        date: dateChoose,
        dateStr: dateChoose.year + "-" + dateChoose.month + "-" + dateChoose.day,
        recent: ''
      }

      const startDateChoose = this.dateFormat(this.data.startDates)
      const endDateChoose = this.dateFormat(this.data.endDates)
      const startDateStr = startDateChoose.year + "-" + startDateChoose.month + "-" + startDateChoose.day
      const endDateStr = endDateChoose.year + "-" + endDateChoose.month + "-" + endDateChoose.day
      const choose2 = {
        startDateTime: this.data.startDates,
        endDateTime: this.data.endDates,
        startDate: startDateChoose,
        endDate: endDateChoose,
        startDateStr: startDateStr,
        endDateStr: endDateStr,
        startRecent: '',
        endRecent: ''
      }



      choose2.countDays = (this.data.endDates * 1 - this.data.startDates * 1) / 86400 / 1000;

      if (this.mode == 2) { //往返模式
        if (this.data.startDates && !this.data.endDates) { //单日往返
          choose2.endDate = choose2.startDate
          choose2.endDateStr = choose2.startDateStr
          choose2.endDateTime = choose2.startDateTime
          choose2.endRecent = choose2.startRecent

          this.emitEvent(choose2, true)
        } else if (this.data.startDates) { //去程-返程
          this.emitEvent(choose2)
        }
      } else { //酒店模式
        if (this.data.startDates && this.data.endDates) {
          this.emitEvent(choose2, false)
        }
      }
      this.matchCurrent();
    },
    addBetweenStyle: function(day, month, year) {
      if (!day || this.data.date) {
        return '';
      }
      const _date = new Date(year + '/' + month + '/' + day) * 1
      if (_date >= this.data.startDates * 1 && _date <= this.data.endDates * 1) {
        return this.data.betweenColor
      } else {
        return ''
      }
    },
    matchCurrent: function() {
      const calendar = this.data.calendar;
      for (const i in calendar) {
        for (const j in calendar[i].dayList) {
          const day = calendar[i].dayList[j].day;
          const month = calendar[i].month;
          const year = calendar[i].year;
          calendar[i].dayList[j] = Object.assign(calendar[i].dayList[j], {
            clickTime: this.isCurrent(day, month, year),
            betweenStyle: this.addBetweenStyle(day, month, year),
            tip: this.setTip(day, month, year),

          })
        }
      }
      this.setData({
        calendar: calendar
      })

    },
    //返回recent
    setRecent: function(_date, choose, recent) {
      if (_date == this.data.today) {
        choose[recent] = '今天'
      } else if (_date - this.data.today == 24 * 3600 * 1000) {
        choose[recent] = '明天'
      } else if (_date - this.data.today == 2 * 24 * 3600 * 1000) {
        choose[recent] = '后天'
      }
    },
    isCurrent: function(day, month, year) {
      if (!day) {
        return false;
      }

      const _date = new Date(year + '/' + month + '/' + day) * 1
      //设置开始和结束
      if (_date === this.data.startDates * 1 || (_date === this.data.endDates * 1)) {
        return true
      }
      return false
    },
    //返回recent
    setRecent: function(_date, choose, recent) {
      if (_date == this.data.today) {
        choose[recent] = '今天'
      } else if (_date - this.data.today == 24 * 3600 * 1000) {
        choose[recent] = '明天'
      } else if (_date - this.data.today == 2 * 24 * 3600 * 1000) {
        choose[recent] = '后天'
      }
    },
    emitEvent: function(choose, isWf = false) {
      if (isWf) {
        this.setRecent(this.startDates, choose, 'startRecent')
        this.setRecent(this.startDates, choose, 'endRecent')
      } else {
        this.setRecent(this.startDates, choose, 'startRecent')
        this.setRecent(this.endDates, choose, 'endRecent')
      }
      this.triggerEvent('complete', choose, {})
    },
    //农历转化方法
    sloarToLunar: function(day, month, year) {
      month -= 1;
      // 计算与公历基准的相差天数
      // Date.UTC()返回的是距离公历1970年1月1日的毫秒数,传入的月份需要减1
      let daySpan = (Date.UTC(year, month, day) - Date.UTC(1949, 0, 29)) / (24 * 60 * 60 * 1000) + 1;
      let ly, lm, ld;
      // 确定输出的农历年份
      const lunarYearArr = this.data.lunarYearArr
      for (let j = 0; j < lunarYearArr.length; j++) {
        daySpan -= this.lunarYearDays(lunarYearArr[j]);
        if (daySpan <= 0) {
          ly = 1949 + j;
          // 获取农历年份确定后的剩余天数
          daySpan += this.lunarYearDays(lunarYearArr[j]);
          break
        }
      }
      // 确定输出的农历月份
      for (let k = 0; k < this.lunarYearMonths(lunarYearArr[ly - 1949]).length; k++) {
        daySpan -= this.lunarYearMonths(lunarYearArr[ly - 1949])[k];
        if (daySpan <= 0) {
          // 有闰月时，月份的数组长度会变成13，因此，当闰月月份小于等于k时，lm不需要加1
          if (this.hasLeapMonth(lunarYearArr[ly - 1949]) && this.hasLeapMonth(lunarYearArr[ly - 1949]) <= k) {
            if (this.hasLeapMonth(lunarYearArr[ly - 1949]) < k) {
              lm = k;
            } else if (this.hasLeapMonth(lunarYearArr[ly - 1949]) === k) {
              lm = '闰' + k;
            } else {
              lm = k + 1;
            }
          } else {
            lm = k + 1;
          }
          // 获取农历月份确定后的剩余天数
          daySpan += this.lunarYearMonths(lunarYearArr[ly - 1949])[k];
          break
        }
      }
      ld = daySpan;

      // 将计算出来的农历年份转换为天干地支年
      const monthSpan = lm;
      const yearSpan = ly;
      ly = this.getTianGan(ly) + this.getDiZhi(ly);


      // 将计算出来的农历月份转换成汉字月份，闰月需要在前面加上闰字
      if (this.hasLeapMonth(lunarYearArr[ly - 1949]) && (typeof(lm) === 'string' && lm.indexOf('闰') > -1)) {
        lm = `闰${this.data.lunarMonth[/\d/.exec(lm) - 1]}`
      } else {
        lm = this.data.lunarMonth[lm - 1];
      }
      // 将计算出来的农历天数转换成汉字
      if (ld < 11) {
        ld = `${this.data.lunarDay[10]}${this.data.lunarDay[ld - 1]}`
      } else if (ld > 10 && ld < 20) {
        ld = `${this.data.lunarDay[9]}${this.data.lunarDay[ld - 11]}`
      } else if (ld === 20) {
        ld = `${this.data.lunarDay[1]}${this.data.lunarDay[9]}`
      } else if (ld > 20 && ld < 30) {
        ld = `${this.data.lunarDay[11]}${this.data.lunarDay[ld - 21]}`
      } else if (ld === 30) {
        ld = `${this.data.lunarDay[2]}${this.data.lunarDay[9]}`
      }
      const lunarResult = {
        lunarYear: ly,
        lunarMonth: lm,
        lunarDay: ld,
        daySpan: daySpan,
        monthSpan: monthSpan,
        yearSpan: yearSpan
      }
      return lunarResult;
    },
    // 计算农历一年的总天数，参数为存储农历年的16进制
    // 农历年份信息用16进制存储，其中16进制的第2-4位（0x除外）可以用于表示正常月是大月还是小月
    lunarYearDays: function(ly) {
      let totalDays = 0;

      // 获取正常月的天数，并累加
      // 获取16进制的第2-4位，需要用到>>移位运算符
      for (let i = 0x8000; i > 0x8; i >>= 1) {
        let monthDays = (ly & i) ? 30 : 29;
        totalDays += monthDays;
      }
      // 如果有闰月，需要把闰月的天数加上
      if (this.hasLeapMonth(ly)) {
        totalDays += this.leapMonthDays(ly);
      }

      return totalDays
    },
    // 计算农历年是否有闰月，参数为存储农历年的16进制
    // 农历年份信息用16进制存储，其中16进制的最后1位可以用于判断是否有闰月
    hasLeapMonth: function(ly) {
      // 获取16进制的最后1位，需要用到&与运算符
      if (ly & 0xf) {
        return ly & 0xf
      } else {
        return false
      }
    },
    // 如果有闰月，计算农历闰月天数，参数为存储农历年的16进制
    // 农历年份信息用16进制存储，其中16进制的第1位（0x除外）可以用于表示闰月是大月还是小月
    leapMonthDays: function(ly) {
      if (this.hasLeapMonth(ly)) {
        // 获取16进制的第1位（0x除外）
        return (ly & 0xf0000) ? 30 : 29
      } else {
        return 0
      }
    },
    // 获取农历每个月的天数
    // 参数需传入16进制数值
    lunarYearMonths: function(ly) {
      let monthArr = [];

      // 获取正常月的天数，并添加到monthArr数组中
      // 获取16进制的第2-4位，需要用到>>移位运算符
      for (let i = 0x8000; i > 0x8; i >>= 1) {
        monthArr.push((ly & i) ? 30 : 29);
      }
      // 如果有闰月，需要把闰月的天数加上
      if (this.hasLeapMonth(ly)) {
        monthArr.splice(this.hasLeapMonth(ly), 0, this.leapMonthDays(ly));
      }

      return monthArr
    },
    // 将农历年转换为天干，参数为农历年
    getTianGan: function(ly) {
      let tianGanKey = (ly - 3) % 10;
      if (tianGanKey === 0) tianGanKey = 10;
      return this.data.tianGan[tianGanKey - 1]
    },

    // 将农历年转换为地支，参数为农历年
    getDiZhi: function(ly) {
      let diZhiKey = (ly - 3) % 12;
      if (diZhiKey === 0) diZhiKey = 12;
      return this.data.diZhi[diZhiKey - 1]
    }
  },

})