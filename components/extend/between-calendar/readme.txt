版本0.0.3
参数
1、themeColor：日历的主题色，例:themeColor="'#FF6600'"(默认#ff4400)
2、paddindTop：日历本体距离顶部那边距，(默认80rpx)
3、height：日历总体高度，(默认100vh)
4、initMonthCount 要初始多少个月份（默认6个月）最小1个月
5、date：传入初始日期（默认当天）
6、startDate：酒店\往返模式的入住日期
7、endDate：酒店\往返模式的离开日期
8、disabledList：设置不可点击的日期
9、mode：模式选择（默认1），酒店模式，2往返模式
10、allAbled 解除所有disabled的限制，包含今天之前所有日期均都可操作

