const App = getApp();
// pages/store/detail.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    id:'', // 详情id
    info:'', // 信息数据
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
     const that=this;
     that.setData({
       id: options.id,
     })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
   const that=this;
    that.detail();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  /**
   * 详情数据
   */
  detail:function(){
    const that=this;
    wx.getLocation({
      type: 'gcj02',
      success(res) {
        const latitude = res.latitude
        const longitude = res.longitude
        App._get('moving.cooperation/detail', {
          shop_id: that.data.id,
          longitude: longitude || '',
          latitude: latitude || ''
        }, (result) => {
          that.setData({
            info: result.data,
          });
        });
        // const speed = res.speed
        // const accuracy = res.accuracy
      },
      fail(res) {
        App._get('moving.cooperation/detail', {
          shop_id:that.data.id,
          longitude: '',
          latitude: ''
        }, (result) => {
          that.setData({
            info: result.data,
          });
        });
      }
    })
  },

  /**
   * 点击导航
   */
  toGetLocation:function(){
    const that=this;
        wx.openLocation({
          latitude: that.data.info.latitude*1,
          longitude: that.data.info.longitude*1,
          scale: 18
        })
      },

/**
 * 拨打电话
 */
  callTel:function(){
    const that=this;
    wx.makePhoneCall({
      phoneNumber: that.data.info.phone //仅为示例，并非真实的电话号码
    })
  }
      
})