const App = getApp();
// pages/store/list.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    shopList:'', // 门店列表 
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    const that=this;
    that.storeLists();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  /**
   * 商家列表
   */
  storeLists:function(){
    const that = this;
    wx.getLocation({
      type: 'gcj02',
      success(res) {
        const latitude = res.latitude
        const longitude = res.longitude
        App._get('shop/lists', {
          longitude: longitude || '',
          latitude: latitude || ''
        }, (result) => {
          that.setData({
            shopList: result.data.list,
          });
        });
        // const speed = res.speed
        // const accuracy = res.accuracy
      },
      fail(res){
        App._get('shop/lists', {
          longitude: '',
          latitude: ''
        }, (result) => {
          that.setData({
            shopList: result.data.list,
          });
        });
      }
    })
    
  },

  /**
   * 前往商家详情
   */
  toDetail:function(e){
    console.log(e);
    let id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: `/pages/store/detail?id=${id}`,
    })
  }

})