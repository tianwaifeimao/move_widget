const App = getApp();
// pages/agent/home.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
      info:'',//商家信息
    shop:0,//商家状态
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    const that=this;
    that.center();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  /**
   * 合作商信息
   */
  center:function(){
     const that=this;
    App._post_form('moving.cooperation/center',{},(res)=>{
      that.setData({
        info:res.data.list[0],
        shop: res.data.is_shop
      })  
    })
  },

  /**
   * 前往商家订单
   */
  toOrder:function(){
    const that=this;
     wx.navigateTo({
       url: `/pages/store/order?id=${that.data.info.shop_id}`,
     })
  },

  /**
   * 跳转到提现页面
   */
  navigationToWithdraw: function () {
    const that=this;
    wx.navigateTo({
      url: `/pages/store/withdraw/apply/apply?id=${that.data.info.shop_id}`,
    })
  },

  /**
   * 前往明细
   */
  toWithdrawDetail:function(){
    const that=this;
    wx.navigateTo({
      url: `/pages/store/withdraw/list/list?id=${that.data.info.shop_id}`,
    })
  },

  /**
   * 立即加入分销商
   */
  triggerApply: function (e) {
    // 记录formId
    App.saveFormId(e.detail.formId);
    wx.navigateTo({
      url: '/pages/agent/index',
    })
  },
})