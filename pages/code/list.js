const App = getApp();
// pages/code/list.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    id:'', // 当前选择id
    list:'', // 我的挪车码列表
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
   const that=this;
   that.lists();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  /**
   * 打开编辑
   */
  openMenu:function(e){
    const that=this;
    const id = e.currentTarget.dataset.id;
    console.log(id);
    wx.showActionSheet({
      itemList: ['删除', '编辑', '查看挪车码'],
      success(res) {
        switch (res.tapIndex){
          case 0: App.showModal('确认删除？',()=>{
            App._post_form('moving.car/delBindCar', {car_id:id},()=>{
                    wx.showToast({
                      title: '删除成功',
                    })
              that.lists();
                })
              }); break;
          case 1: 
            wx.navigateTo({
              url: `/pages/apply/addcar?id=${id}`,
            })
           break;
              case 2 :wx.navigateTo({
            url: `/pages/code/index?id=${id}`,
              });
              break;
        }
      },
      fail(res) {
        console.log(res.errMsg)
      }
    })
  },

  /**
   * 
   */
  lists:function(){
    const that=this;
    App._post_form('moving.car/lists', {page:1},(res)=>{
      that.setData({
        list:res.data.list.data
      })
    })
  }
})