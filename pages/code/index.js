const App = getApp();
// pages/code/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    images:[],
    index:0,
    carId:'', // 车辆id
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const that=this;
    that.setData({
      carId:options.id,
    })
    that.styleLists();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  /**
   * 切换图片
   */
  switchPic:function(e){
     const type = e.currentTarget.dataset.type;
     const that=this;
     if(type=='left'){
       if(that.data.index==0){
         that.setData({
           index: that.data.images.length*1 - 1
         })
       }else{
         that.setData({
           index: that.data.index * 1 - 1
         })
       }
     } else if (type == 'right'){
       if (that.data.index == that.data.images.length * 1 - 1) {
         that.setData({
           index: 0
         })
       } else {
         that.setData({
           index: that.data.index * 1 + 1
         })
       }
     }
     
  },

  /**
   * 拖动改变图片事件
   */
  changePic:function(e){
    const that=this;
    that.setData({
      index: e.detail.current
    })
  },

  /**
   * 生成打开图片
   */
  openImage:function(){
    const that=this;
    that.createQrcode();
  },

  /**
   * 挪车码样式
   */
  styleLists:function(){
    const that=this;
    App._post_form('moving.qrcode/styleLists',{},(res)=>{
       that.setData({
         images:res.data.lists
       })
    })
  },

  /**
   * 生成挪车码接口
   */
  createQrcode:function(){
    const that=this;
    App._post_form('moving.qrcode/createQrcode', { 
      car_id:that.data.carId,
      style_id: that.data.images[that.data.index].style_id
      },(res)=>{
        wx.previewImage({
          current: res.data.qrcode.code_file, // 当前显示图片的http链接
          urls: [res.data.qrcode.code_file] // 需要预览的图片http链接列表
        })
    })
  }
})