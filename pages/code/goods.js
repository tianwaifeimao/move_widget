const App = getApp();
// pages/code/goods.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list:[], // 二维码样式列表
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    const that=this;
    that.showQrcodeStyle();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  /**
   * 获取二维码列表
   */
  showQrcodeStyle:function(){
    const that=this;
    App._post_form('moving.qrcode/showQrcodeStyle',{},(res)=>{
       that.setData({
         list:res.data.list,
       })
    })
  },

  /**
   * 前往购买
   */
  toBuy:function(e){
    let id=e.currentTarget.dataset.id;
    wx.navigateTo({
      url: `/pages/flow/checkout?goods_id=${id}&goods_num=1&goods_sku_id=0&order_type=buyNow`,
    })
  }
})