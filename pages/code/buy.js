const App = getApp();

// 工具类
import Util from '../../utils/util.js';

// 验证类
import Verify from '../../utils/verify.js';

// 枚举类：发货方式
import DeliveryTypeEnum from '../../utils/enum/DeliveryType.js';

// 枚举类：支付方式
import PayTypeEnum from '../../utils/enum/order/PayType';

// 对话框插件
import Dialog from '../../components/dialog/dialog';

Page({

  /**
   * 页面的初始数据
   */
  data: {

    // 当前页面参数
    options: {},
    id:'', // 车辆id

    // // 系统设置：配送方式
    // deliverySetting: [],

    // 系统设置
    setting: {
      delivery: [], // 支持的配送方式
    },

    // 配送方式
    isShowTab: false,
    DeliveryTypeEnum,
    curDelivery: 10,

    // 支付方式
    PayTypeEnum,
    curPayType: PayTypeEnum.WECHAT.value,

    address: null, // 默认收货地址
    exist_address: false, // 是否存在收货地址

    selectedShopId: 0, // 选择的自提门店id
    linkman: '', // 自提联系人
    phone: '', // 自提联系电话

    // 商品信息
    goods: {},

    // 选择的优惠券
    selectCouponId: 0,

    // 是否使用积分抵扣
    isUsePoints: false,

    // 买家留言
    remark: '',

    // 禁用submit按钮
    disabled: false,

    hasError: false,
    error: '',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    let _this = this;
    // 当前页面参数
    _this.setData(
      options
    );
    console.log(options);
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    let _this = this;
    _this.buyQrcode();
  },

  /**
   * 快递配送：选择收货地址
   */
  onSelectAddress() {
    wx.navigateTo({
      url: '../address/' + (this.data.address ? 'index?from=flow' : 'create')
    });
  },


  /**
   * 订单提交
   */
  onSubmitOrder() {
    let that = this;

    if (that.data.disabled) {
      return false;
    }

    // 按钮禁用, 防止二次提交
    that.data.disabled = true;

    // 显示loading
    wx.showLoading({
      title: '正在处理...'
    });
    // 订单提交
    that.buyQrcodeReal();

  },

  /**
   * 获取商品详情
   */
  buyQrcode:function(){
    const that=this;
    App._get('moving.run/buyGoods', { goods_id: that.data.id, goods_num: 1, goods_sku_id: 0},(res)=>{
      that.setData(res.data)
    })
  },

   /**
   * 购买商品
   */
  buyQrcodeReal: function () {
    const that = this;
    
    App._post_form('moving.run/buyGoods', { goods_id: that.data.id, goods_num: 1, goods_sku_id: 0}, (result) => {
      App.wxPayment({
        payment: result.data.payment,
        success: res => {
          that.redirectToOrderIndex();
        },
        fail: res => {
          App.showError(result.msg.error, () => {
            that.redirectToOrderIndex();
          });
        },
        complete:res =>{
          wx.hideLoading();
          // 解除按钮禁用
          that.data.disabled = false;
        }
      });
    })
  },

  /**
   * 跳转到未付款订单
   */
  redirectToOrderIndex() {
    wx.redirectTo({
      url: '../order/index',
    });
  },
});