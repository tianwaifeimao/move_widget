const App = getApp();
// pages/index/company.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    headImage: App.api_url+ 'assets/api/aboutus.jpg',
    nav:'',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.aboutus();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
   
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  /**
   * 页面数据
   */
  aboutus:function(){
    const that=this;
    App._post_form('page/aboutus',{},(res)=>{
       that.setData({nav:res.data})
    })
  },

  /**
   * 跳转
   */
  goTo:function(e){
    const that=this;
    let type = e.currentTarget.dataset.type;
    wx.navigateTo({
      url: '/'+that.data.nav[type],
    })
  }
})