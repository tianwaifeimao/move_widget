const App = getApp();
// pages/sweep/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    code: '', // 挪车码编号
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    console.log(options);
    const that=this;
    let code = decodeURIComponent(options.scene).split(':')[1];
    that.setData({
      code,
    })
    this.scanQrcode();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },

  /**
   * 判断扫描挪车码状态，进行对应的操作
   */
  scanQrcode: function() {
    const that = this;
    App._post_form('moving.qrcode/scanQrcode', {
      code_no: that.data.code
    }, (res) => {
      if (res.data.is_bind==1){
          that.setData(res.data)
      }else{
          wx.navigateTo({
            url: `/pages/apply/addcar?code=${that.data.code}`,
          })
      }
    })
  },

  /**
   * 微信通知
   */
  noticeByWx:function(){
    const that=this;
    wx.getLocation({
      type: 'gcj02',
      success(res) {
        const latitude = res.latitude
        const longitude = res.longitude
        App._post_form('moving.qrcode/noticeByWx', { 
          code_no: that.data.code ,
          longitude: longitude,
          latitude: latitude
          }, (res) => {
          wx.showToast({
            title: '已微信通知车主！',
            icon: 'none',
            duration: 2000,
          });
        })
        // const speed = res.speed
        // const accuracy = res.accuracy
      },
      fail(res) {
        App._post_form('moving.qrcode/noticeByWx', {
          code_no: that.data.code,
          longitude: '',
          latitude: ''
        }, (res) => {
          wx.showToast({
            title: '已微信通知车主！',
            icon: 'none',
            duration: 2000,
          });
        })
      }
    })
    
  },

  /**
   * 表单提交，获取formid
   */
  formSubmit:function(e){
    let formId = e.detail.formId;
    this.noticeByWx();
  },

  /**
   * 电话通知挪车
   */
  noticeByTel:function(){
    const that=this;
    App._post_form('moving.qrcode/noticeByTel',{
      code_no: that.data.code,
    },(res)=>{
      wx.makePhoneCall({
        phoneNumber: res.data.tel,
      })
    })
  }
})