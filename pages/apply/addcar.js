const App = getApp();
// pages/apply/addcar.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: ['京', '津', '沪', '渝', '冀', '豫', '云', '辽', '黑', '湘', '皖', '鲁', '新', '苏', '浙', '赣', '鄂', '桂', '甘', '晋', '蒙', '陕', '吉', '闽', '贵', '粤', '青', '藏', '川', '宁','琼'], // 车牌首字母数据
    cartype:'闽', // 当前车牌首字母
    listShow:false, // 首字母菜单显示隐藏
    cateId:0,//车辆id，编辑时存在，添加为0
    detail:{
      car_no:'',
      contact_user:'',
      contact_tel:'',
    },
    code:'', // 挪车码编号，扫码进入时存在
    codeImage: '' ,// 公众号二维码
    showCode:false, // 公众号二维码显示隐藏
    tel:'', // 当前手机号
    title: '获取验证码',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const that=this;
    if (options.code){
      that.setData({
        code:options.code
      })
    }
    if(options.id){
      that.setData({
        cateId: options.id
      })
      that.getBindCar();
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
   const that=this;
    that.checkSubscribe();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  /**
   * 字母菜单显示隐藏
   */
  menuToggle:function(){
     const that=this;
     if(that.data.listShow){
       that.setData({
         listShow: false
       })
     }else{
       that.setData({
         listShow: true
       })
     }
  },

  /**
   * 车辆首字母选择
   */
  typeSelect:function(e){
    const that=this;
    let idx = e.currentTarget.dataset.idx
    that.setData({
      cartype:that.data.list[idx],
      listShow:false
    })
  },

  /**
   * 添加编辑车辆信息
   */
  bindCar:function(e){
    const that=this;
    let value = { ...e.detail.value, ...{ car_id: that.data.cateId, area: that.data.cartype, code_no:that.data.code}};
    App._post_form('moving.car/bindCar',value,(res)=>{
        wx.showToast({
          title: '绑定成功',
        })
        setTimeout(()=>{
          wx.navigateTo({
            url: '/pages/code/list',
          })
        },1500)
    })
  },

  /**
   * 获取车辆详情
   */
  getBindCar:function(){
     const that=this;
    App._get('moving.car/bindCar', { car_id:that.data.cateId},(res)=>{
      that.setData({
        detail: res.data.detail,
        cartype: res.data.detail.area,
        tel: res.data.detail.contact_tel
      })
     })
  },

  /**
   * 验证是否关注公众号
   */
  checkSubscribe:function(){
   const that=this;
    App._post_form('moving.car/checkSubscribe',{},(res)=>{
      if (res.data.is_subscribe==0){
        that.setData({
          showCode:true,
          codeImage: res.data.wechat_qrcode,
        })
      }
    })
  },

  /**
   * 打开二维码图片
   */
  previewImage:function(){
    const that=this;
    wx.previewImage({
      current: that.data.codeImage, // 当前显示图片的http链接
      urls: [that.data.codeImage] // 需要预览的图片http链接列表
    })
  },

  //手机号值变化
  telchange(e) {
    const that = this;
    that.setData({
      tel: e.detail.value
    })
  },

  /**
   * 获取验证码
   */
  gaincode() {
    const that = this;
    if (that.data.title == '获取验证码') {

      App._post_form('sms/sendCode', { mobile: that.data.tel}, (res) => {
        that.setData({
          title: 59
        })
        let time = setInterval(() => {
          that.setData({
            title: that.data.title - 1
          })
          if (that.data.title == 0) {
            clearInterval(time)
            that.setData({
              title: '获取验证码'
            })
          }
        },
          1000);
      })


    }
  },
})