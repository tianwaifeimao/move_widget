const App = getApp();
// pages/user/step/poster.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
     image:'' // 步赚海报图片
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.runPoster();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  /**
   * 用户海报接口
   */
  runPoster:function(){
    const that=this;
    App._post_form('moving.run/runPoster',{},(res)=>{
         that.setData({
           image:res.data.data,
         })
    })
  },

  /**
   * 打开图片
   */
  previewImage:function(){
    const that=this;
    wx.previewImage({
      current: that.data.image, // 当前显示图片的http链接
      urls: [that.data.image] // 需要预览的图片http链接列表
    })
  }
})