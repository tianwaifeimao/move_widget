const App = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    isLoading: true,
    dataType: 1,
    page: 1,
    no_more: false,
    list:[],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // 获取我的团队列表
    this.getTeamList();
  },

  /**
   * 获取我的团队列表
   */
  getTeamList: function (isNextPage, page) {
    let _this = this;
    App._get('moving.run/getGiftOrign', {
      page: page || 1,
    }, function (result) {
  
        _this.setData(_this.createData(result.data, isNextPage));
    
    });
  },

  /**
   * 创建页面数据
   */
  createData: function (data, isNextPage) {
    data['isLoading'] = false;
    // 列表数据
    let dataList = this.data.list;
    if (isNextPage == true && (typeof dataList !== 'undefined')) {
      data.list.data = dataList.data.concat(data.list.data)
    }
    return data;
  },

  /**
   * 下拉到底加载数据
   */
  triggerDownLoad: function () {
    // console.log(this.data.list);
    // 已经是最后一页
    if (this.data.page >= this.data.list.last_page) {
      this.setData({
        no_more: true
      });
      return false;
    }
    this.getTeamList(true, ++this.data.page);
  },

  

})