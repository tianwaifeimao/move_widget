const App = getApp();
// pages/user/step/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    code:'',//微信code
    encryptedData: '',//加密数据
    iv: '',//加密数据
    step: '',//今日步数
    total:'',//剩余可兑换步数
    rate:'', // 步数兑换比例
    stepValue:'', // 兑换步数表单值
    scaleShow:false, // 兑换积分框显示隐藏
    goods:'', // 商品数据
    data:[], // 页面数据
    reffer:[],//推荐人数组
    noreffer:[], //没有推荐人数组 
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const that=this;
    if (options.scene){
      let pid = decodeURIComponent(options.scene).split(':')[1];
      wx.setStorageSync('pid', pid)
    } else if (options.pid){
      wx.setStorageSync('pid', options.pid)
    }
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    App.getSubscribeTpID().then(res=>{
      console.log(res);
    })
    const that=this;
    if (wx.getStorageSync('pid') && wx.getStorageSync('token')) {
      that.runGift(wx.getStorageSync('pid'));
      wx.removeStorageSync('pid');
    }
    
    wx.login({
      success: function (res) {
        that.setData({
          code: res.code
        })
        wx.getWeRunData({
          success(result) {
            wx.showLoading({
              title: '加载中...',
            })
            that.setData({
              encryptedData: result.encryptedData,
              iv: result.iv
            })
            that.syncRunData();
            if(wx.getStorageSync('token')){
              that.getUserDetail();
            }
          },
          complete(){
        
          }
        })
      }
    })
    // that.codeScale();
    that.getExchangeGoods();

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    const that=this;
    return {
      title: '邀请您一起步数换购！',
      path: '/pages/user/step/index?pid=' + that.data.userInfo.user_id,
    }
  },

  /**
   * 获取步数
   */
  syncRunData(){
    const that=this;
    App._post_form('moving.mini/syncRunData', {
      code: that.data.code,
      encryptedData: that.data.encryptedData,
      iv: that.data.iv
    }, function (res) {
      const arr = res.data.giftData.giftOrign.data
      let reffer=[];
      let noreffer=[];
      if(arr.length){
        for(let i=0; i<5;i++){
           if(arr[i]){
             reffer.push(arr[i])
           }else{
             noreffer.push('')
           }
        }
      }else{
        noreffer=['','','','','']
      }
      that.setData({
        reffer,
        noreffer,
        step: res.data.today,
        total: res.data.total,
        data: res.data.giftData
      })
      wx:wx.hideLoading();
      // _this.setData(result.data);
    },()=>{
      wx:wx.hideLoading();
    });
  },

  /**
   * 获取积分兑换比例
   */
  codeScale:function(){
     const that=this;
    App._get('moving.run/submit',{},(res)=>{
        that.setData({
          rate:res.data.rate
        })
     })
  },

  /**
   * 获取兑换积分表单值
   */
  stepChange:function(e){
    const that=this;
     let step=e.detail.value;
     that.setData({
       stepValue:step,
     })
     
  },

  /**
   * 兑换积分
   */
  exChange:function(){
    const that=this;
    App._post_form('moving.run/submit', { step: that.data.stepValue}, (res) => {
      that.setData({
        scaleShow:false,
      })
      App.showSuccess('兑换成功！');
      that.onShow();
    })
  },

  /**
   * 显示隐藏兑换框
   */
  scaleToggle:function(){
    const that=this;
    if(that.data.scaleShow){
      that.setData({
        scaleShow: false,
      })
    }else{
      that.setData({
        scaleShow: true,
      })
    }
   
  },

  /**
   * 存入账户
   */
  saveStep:function(){
   wx.showLoading({
     title: '加载中...',
   })
     const that=this;
    wx.login({
      success: function (res) {
        that.setData({
          code: res.code
        })
        wx.getWeRunData({
          success(result) {
            App._post_form('moving.mini/saveRunData', {
              code: res.code,
              encryptedData: result.encryptedData,
              iv: result.iv
            }, (info) => {
              that.setData({
                total:info.data.total,
                step:0
              })
              App.showSuccess('存入成功！')
            })
          },
          fail() {
            App.showSuccess('存入失败！')
          },
          complete(){
            wx.hideLoading()
          }
        })
      }
    })
   
  },

  /**
   * 获取商品信息
   */
  getExchangeGoods:function(){
    const that=this;
    App._post_form('moving.run/getExchangeGoods',{
    },(res)=>{
      that.setData({
        goods:res.data.list.data
      })
    })
  },

  /**
   * 前往详情
   */
  toDetail:function(e){
    console.log(e)
    let id=e.currentTarget.dataset.id
    const that=this;
    wx.navigateTo({
      url: `/pages/user/step/goods?goods_id=${id}`,
    })
  },

  /**
   * 步数兑换余额
   */
   stepExh:function(){
     const that=this;
     App._post_form('moving.run/stepExh',{},(res)=>{
       App.showSuccess('兑换成功，已存入余额！')
       that.setData({
         total: res.data.total
       })
     })
   },

   /**
    * 赠送推荐步数
    */
    runGift:function(pid){
      const that=this;
      App._post_form('moving.run/runGift',{pid,},(res)=>{
      
      })
    },

    /**
     * 领取推荐步数
     */
    getGift:function(){
       const that=this;
      App._post_form('moving.run/getGift',{},(res)=>{
         App.showSuccess('领取成功！')
         let data=that.data.data
          data.todayGift=0;
         that.setData({
           data,
         })
       })
    },

  /**
 * 获取当前用户信息
 */
  getUserDetail() {
    let _this = this;
    App._get('user.index/detail', {}, function (result) {
      _this.setData(result.data);
    });
  },

  
  
})